import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService{
  isAuthenticated(): boolean {
    return this._userIsAuthenticated;
  }

  public observableIsAuthenticated : BehaviorSubject<boolean>;
  private _userIsAuthenticated = false;

  constructor(
    private router : Router
  ) {
    // this.observableIsAuthenticated = new Observable();
    this.observableIsAuthenticated = new BehaviorSubject(false);
    this.observableIsAuthenticated.subscribe(val => {
      console.log("val", val);
      
      this._userIsAuthenticated = val;
      if(this._userIsAuthenticated === false)
        this.router.navigateByUrl("/auth");
    });
    
   }

  login(){
    this.observableIsAuthenticated.next(true);
  }

  logout(){
    this.observableIsAuthenticated.next(false);
  }

  getUser(){
    if(this._userIsAuthenticated)
    {
      return { token : "zefzfzefzefzefzefzefzf" };
    }
  }
}
