import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Place } from '../../place.model';
import { PlacesService } from '../../places.service';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit {

  public place : Place;

  constructor(
    private route : ActivatedRoute,
    private placesService : PlacesService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      console.log(params.get("placeId"));
      
      if(params.has('placeId')){
        this.place = this.placesService.getPlace(
          params.get("placeId")
        );
      }
      else{
        
      }
    })
  }


}
