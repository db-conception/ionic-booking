import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  

  private _places : Place[] = [
    new Place("p1", "Champs Elysées", "Au coeur de Paris", "https://medias.fashionnetwork.com/image/upload/c_limit,f_auto,h_963,q_auto:best,w_963/v1/medias/8b4ac9e28b590c61f113e08ddf4475822462784.jpg", 1200),
    new Place("p2", "Place Gralin", "Au coeur de Nantes", "https://voyages.michelin.fr/sites/default/files/styles/poi_slideshow/public/images/travel_guide/NX-43913.jpg", 300),
    new Place("p3", "Place fictive", "N'existe pas", "https://medias.fashionnetwork.com/image/upload/c_limit,f_auto,h_963,q_auto:best,w_963/v1/medias/8b4ac9e28b590c61f113e08ddf4475822462784.jpg", 5)
  ];
  constructor() { }

  getPlaces(){
    return [...this._places];
  }

  getPlace(id: string) {
    console.log("find", this._places.find(p => p.id === id));
    return {...this._places.find(p => p.id === id)};
  }
}
