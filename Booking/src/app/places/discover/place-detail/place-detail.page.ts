import { registerLocaleData } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { CreateBookingComponent } from 'src/app/bookings/create-booking/create-booking.component';
import { Place } from '../../place.model';
import { PlacesService } from '../../places.service';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit {

  place:  Place;
  constructor(private router : Router,
    private navCtrl : NavController,
    private placesService: PlacesService,
    private route : ActivatedRoute,
    private modalCtrl : ModalController) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      console.log(params.get("placeId"));
      
      if(params.has('placeId')){
        this.place = this.placesService.getPlace(
          params.get("placeId")
        );
      }
      else{
        
      }
    })
  }

  onBookPlace(){

    this.modalCtrl.create({
      component: CreateBookingComponent,
      animated: true,
      keyboardClose: true,
      componentProps : { selectedPlace : this.place}
    }).then(modal => {
        modal.present(); 
        return modal.onDidDismiss();
      }).then(resultData => {
        console.log(resultData);
      });

    // this.router.navigateByUrl('/');
    // this.navCtrl.navigateBack('/'); // go to url with back animation
    // this.navCtrl.pop(); // can not garentee that there is previus page
  }
}
